# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 12:54:05 2023

@author: lv
"""
'''
常见的 Unicode 字符集，可以考虑遍历它们：

    Basic Latin（基本拉丁字母）：这个字符集包含了所有 ASCII 字符以及其他一些常用字符，范围是 U+0000 到 U+007F。

    Latin-1 Supplement（拉丁1补充）：这个字符集包含了一些欧洲语言中常用的字符，范围是 U+0080 到 U+00FF。

    Latin Extended-A（拉丁扩展A）：这个字符集包含了一些欧洲语言中较少使用的字符，范围是 U+0100 到 U+017F。

    Latin Extended-B（拉丁扩展B）：这个字符集包含了一些欧洲语言以外的字符，包括一些非洲语言、印第安语言和南亚语言的字符，范围是 U+0180 到 U+024F。

    IPA Extensions（国际音标扩展）：这个字符集包含了国际音标符号，范围是 U+0250 到 U+02AF。

    Greek and Coptic（希腊字母和科普特字母）：这个字符集包含了希腊字母和科普特字母，范围是 U+0370 到 U+03FF。

    Cyrillic（西里尔字母）：这个字符集包含了俄语、乌克兰语、保加利亚语等语言使用的西里尔字母，范围是 U+0400 到 U+04FF。

    Armenian（亚美尼亚字母）：这个字符集包含了亚美尼亚字母，范围是 U+0530 到 U+058F。

    Hebrew（希伯来字母）：这个字符集包含了希伯来字母，范围是 U+0590 到 U+05FF。

    Arabic（阿拉伯字母）：这个字符集包含了阿拉伯字母和其他阿拉伯语字符，范围是 U+0600 到 U+06FF。

    Devanagari（天城文梵文）：这个字符集包含了印度教教义中使用的天城文梵文字符，范围是 U+0900 到 U+097F。

    Thai（泰文）：这个字符集包含了泰文字符，范围是 U+0E00 到 U+0E7F。

    Hangul Jamo（韩文字母）：这个字符集包含了韩文字母，范围是 U+1100 到 U+11FF。

    CJK Unified Ideographs（中日韩统一表意文字）：这个字符集包含了中、日、韩语中使用的汉字，范围是 U+4E00 到 U+9FFF。

'''
import unicodedata

unicode_chars = set()
for i in range(0x110000):
    try:
        char = chr(i)
        name = unicodedata.name(char)
        unicode_chars.add(char)
    except ValueError:
        pass  # 忽略不可打印字符和代理项

# 将字符写入文件
with open("unicode_chars.txt", "w", encoding="utf-8") as f:
    for char in unicode_chars:
        f.write(char + "\n")