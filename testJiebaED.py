# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 11:25:45 2023

@author: lv
"""

from utils.JiebaTokenizer import JiebaTokenizer

encoder_decoder = JiebaTokenizer()
print(encoder_decoder.vocab_size)

text = '这是一个测试句子'
encoded = encoder_decoder.encode(text)
print('编码结果:', encoded)

decoded = encoder_decoder.decode(encoded)
print('解码结果:', decoded)
